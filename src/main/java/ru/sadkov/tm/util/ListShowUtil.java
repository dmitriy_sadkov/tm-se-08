package ru.sadkov.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

import java.util.List;

@UtilityClass
public final class ListShowUtil {
    public static void showList(@Nullable final List<?> list) {
        if (list == null || list.isEmpty()) {
            System.out.println("[EMPTY]");
            return;
        }
        for (Object o : list) {
            System.out.println(o);
        }
    }
}
