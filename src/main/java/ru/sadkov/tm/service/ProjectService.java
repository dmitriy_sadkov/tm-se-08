package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectRepository;
import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.repository.ProjectRepository;
import ru.sadkov.tm.util.RandomUtil;

import java.util.ArrayList;
import java.util.List;

public final class ProjectService implements IProjectService {
    private IProjectRepository projectRepository = new ProjectRepository();

    public ProjectService() {
    }

    @Nullable
    public String findProjectIdByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty() || userId == null || userId.isEmpty()) return null;
        return projectRepository.findProjectIdByName(projectName, userId);
    }

    public boolean saveProject(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty()) return false;
        return projectRepository.merge(new Project(projectName, RandomUtil.UUID(), userId));
    }

    public void removeByName(@Nullable final String projectName, @Nullable final String userId) {
        if (projectName == null || projectName.isEmpty() || userId == null || userId.isEmpty()) return;
        final String projectId = findProjectIdByName(projectName, userId);
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.remove(userId, projectName);
    }

    @Nullable
    public List<Project> showProjectsForUser(@Nullable final User user) {
        if (user == null) return null;
        return new ArrayList<>(projectRepository.findAll(user.getId()));
    }

    public void clearProjects(@Nullable final User user) {
        if (user == null) return;
        projectRepository.removeAll(user.getId());
    }

    @NotNull
    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }

    public void setProjectRepository(@NotNull ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void update(@Nullable final Project project, @Nullable final String projectName, @Nullable final String description) {
        if (project == null || projectName == null || description == null || projectName.isEmpty() || description.isEmpty())
            return;
        projectRepository.update(project.getUserId(), project.getId(), projectName, description);
    }

    @Nullable
    public Project findOneByName(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) return null;
        String projectId = findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return null;
        return projectRepository.findOne(projectId, currentUser.getId());
    }
}
