package ru.sadkov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectService;
import ru.sadkov.tm.api.ITaskRepository;
import ru.sadkov.tm.api.ITaskService;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.repository.TaskRepository;

import java.util.Iterator;
import java.util.List;

public final class TaskService implements ITaskService {
    private ITaskRepository taskRepository = new TaskRepository();
    private IProjectService projectService;

    public TaskService(IProjectService projectService) {
        this.projectService = projectService;

    }
    @NotNull
    public ITaskRepository getTaskRepository() {
        return taskRepository;
    }

    public void setTaskRepository(@NotNull ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Nullable
    public Task findTaskByName(@Nullable final String taskName, @Nullable final String userId) {
        if (taskName == null || taskName.isEmpty() || userId == null || userId.isEmpty()) return null;
        final List<Task> tasks = taskRepository.findAll(userId);
        for (Task task : tasks) {
            if (task.getName().equals(taskName)) {
                return task;
            }
        }
        return null;
    }

    public void saveTask(@Nullable final String taskName, @Nullable final String projectName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty() || projectName == null || projectName.isEmpty() || currentUser == null)
            return;
        final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) {
            return;
        }
        taskRepository.merge(taskName, projectId, currentUser.getId());
    }

    public void removeTask(@Nullable final String taskName, @Nullable final User currentUser) {
        if (taskName == null || taskName.isEmpty()) return;
        if(currentUser==null) return;
        final Task task = taskRepository.findTaskByName(taskName, currentUser.getId());
        if (task == null) return;
        taskRepository.removeByName(taskName, currentUser.getId());
    }

    @Nullable
    public List<Task> findAll(final User user) {
        if (user == null) return null;
        if (taskRepository.findAll(user.getId()).isEmpty()) {
            return null;
        }
        return taskRepository.findAll(user.getId());
    }


    public void clearTasks(@Nullable final User user) {
        if (user == null) return;
        taskRepository.removeAll(user.getId());
    }

    public void removeTaskForProject(@Nullable final String projectName, @Nullable final User currentUser) {
        if (projectName == null || projectName.isEmpty() || currentUser == null) {
            return;
        }
        final String projectId = projectService.findProjectIdByName(projectName, currentUser.getId());
        if (projectId == null || projectId.isEmpty()) return;
        final Project project = projectService.getProjectRepository().findOne(projectId, currentUser.getId());
        if (project == null) return;
        if (project.getUserId().equals(currentUser.getId())) {
            Iterator<Task> iterator = taskRepository.findAll(currentUser.getId()).iterator();
            while (iterator.hasNext()) {
                Task task = iterator.next();
                if (task.getProjectId().equals(projectId)) {
                    taskRepository.removeByName(task.getName(), currentUser.getId());
                }
            }
        }
    }

    public void update(@Nullable final String oldName, @Nullable final String newName, String userId) {
        if (oldName == null || oldName.isEmpty() || newName == null || newName.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        taskRepository.update(oldName, newName, userId);
    }
}

