package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;

public final class ProjectRemoveCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-removeByName";
    }

    @Override
    public String description() {
        return "Remove selected project";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT REMOVE]");
        System.out.println("[ENTER NAME:]");
        final String projectName = serviceLocator.getScanner().nextLine();
        if (projectName == null || projectName.isEmpty() || serviceLocator.getUserService().getCurrentUser() == null) {
            System.out.println("[INCORRECT NAME]");
            return;
        }
        serviceLocator.getTaskService().removeTaskForProject(projectName, serviceLocator.getUserService().getCurrentUser());
        serviceLocator.getProjectService().removeByName(projectName, serviceLocator.getUserService().getCurrentUser().getId());
        System.out.println("[OK]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
