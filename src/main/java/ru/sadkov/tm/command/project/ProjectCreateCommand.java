package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;

public final class ProjectCreateCommand extends AbstractCommand {
    @Override
    public String command() {
        return "project-create";
    }

    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("[ENTER NAME:]");
        final String projectName = serviceLocator.getScanner().nextLine();
        if(serviceLocator.getUserService().getCurrentUser()==null){
            System.out.println("[PLEASE LOGIN]");
            return;
        }
        if (serviceLocator.getProjectService().saveProject(projectName, serviceLocator.getUserService().getCurrentUser().getId())) {
            System.out.println("[OK]");
            return;
        }
        System.out.println("[ERROR]");
    }

    @Override
    public boolean safe() {
        return false;
    }
}
