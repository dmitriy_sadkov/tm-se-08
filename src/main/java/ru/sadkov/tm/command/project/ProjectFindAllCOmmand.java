package ru.sadkov.tm.command.project;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.util.ListShowUtil;

import java.util.List;

public final class ProjectFindAllCOmmand extends AbstractCommand {
    @Override
    public String command() {
        return "project-list";
    }

    @Override
    public String description() {
        return "Show all projects";
    }

    @Override
    public void execute() {
        final List<Project> projectList = serviceLocator.getProjectService().showProjectsForUser(serviceLocator.getUserService().getCurrentUser());
        if (projectList == null || projectList.isEmpty()) {
            System.out.println("[NO PROJECTS]");
            return;
        }
        System.out.println("[PROJECTS:]");
        ListShowUtil.showList(projectList);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
