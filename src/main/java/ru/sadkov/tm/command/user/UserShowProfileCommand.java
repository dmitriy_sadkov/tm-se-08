package ru.sadkov.tm.command.user;

import ru.sadkov.tm.command.AbstractCommand;
import ru.sadkov.tm.entity.User;

public final class UserShowProfileCommand extends AbstractCommand {
    @Override
    public String command() {
        return "show-user";
    }

    @Override
    public String description() {
        return "Show current User profile";
    }

    @Override
    public void execute() {
        System.out.println("[CURRENT USER]");
        if (serviceLocator.getUserService().getCurrentUser() == null) {
            System.out.println("[NO USER! PLEASE LOGIN]");
            return;
        }
        final User currentUser = serviceLocator.getUserService().showUser();
        System.out.println(currentUser);
    }

    @Override
    public boolean safe() {
        return false;
    }
}
