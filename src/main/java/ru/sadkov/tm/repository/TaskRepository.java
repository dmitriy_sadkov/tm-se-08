package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.util.RandomUtil;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ru.sadkov.tm.api.ITaskRepository {
    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        List<Task> result = new ArrayList<>();
        for (Task task : entities.values()) {
            if (task.getUserId().equals(userId)) {
                result.add(task);
            }
        }
        return result;
    }

    public void persist(@NotNull final Task task) {
        entities.put(task.getId(), task);
    }

    public void removeByName(@NotNull final String taskName, @NotNull final String userId) {
        entities.values().removeIf(nextTask -> nextTask.getName().equalsIgnoreCase(taskName) &&
                nextTask.getUserId().equals(userId));
    }

    public void removeAll(@NotNull final String userId) {
        entities.values().removeIf(nextTask -> nextTask.getUserId().equals(userId));
    }

    @Nullable
    public Task findOne(@NotNull final String taskId, @NotNull final String userId) {
        for (Task task : findAll(userId)) {
            if (task.getId().equals(taskId)) {
                return task;
            }
        }
        return null;
    }

    public void merge(@NotNull final String taskName, @NotNull final String projectId, @NotNull final String userId) {
        Task task = findTaskByName(taskName, userId);
        if (task == null) {
            persist(new Task(taskName, RandomUtil.UUID(), projectId, userId));
            return;
        }
        update(taskName, taskName, userId);
    }

    public void update(@NotNull final String oldName, @NotNull final String newName, @NotNull final String userId) {
        Task task = findTaskByName(oldName, userId);
        if (task == null) return;
        task.setName(newName);
    }

    @Nullable
    public Task findTaskByName(@NotNull final String taskName, @NotNull final String userId) {
        for (Task task : findAll(userId)) {
            if (taskName.equals(task.getName())) {
                return task;
            }
        }
        return null;
    }
}
