package ru.sadkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.api.IProjectRepository;
import ru.sadkov.tm.entity.Project;

import java.util.*;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public void persist(@NotNull final Project project) {
        entities.put(project.getId(), project);
    }

    public void remove(@NotNull final String userId, @NotNull final String projectName) {
        entities.values().removeIf(nextProject -> (nextProject.getName().equalsIgnoreCase(projectName)
                && (nextProject.getUserId().equals(userId))));
    }

    @NotNull
    @Override
    public Collection<Project> findAll(@NotNull final String userId) {
        List<Project> result = new ArrayList<>();
        for (Project project : entities.values()) {
            if (project.getUserId().equals(userId)) {
                result.add(project);
            }
        }
        return result;
    }

    @Nullable
    @Override
    public String findProjectIdByName(@NotNull final String name, @NotNull final String userId) {
        for (Project project : findAll(userId)) {
            if (project.getName().equals(name)) {
                return project.getId();
            }
        }
        return null;
    }

    public void removeAll(@NotNull final String userId) {
        entities.values().removeIf(nextProject -> nextProject.getUserId().equals(userId));
    }

    @Nullable
    public Project findOne(@NotNull final String projectId, @NotNull final String userId) {
        for (Project project : findAll(userId)) {
            if (project.getId().equals(projectId)) {
                return project;
            }
        }
        return null;
    }

    public boolean merge(@NotNull final Project project) {
        if (entities.containsValue(project)) {
            update(project.getUserId(), project.getId(), project.getDescription(), project.getName());
            return true;
        } else {
            persist(project);
            return true;
        }
    }

    public void update(@NotNull final String userId,@NotNull  final String id,@NotNull final String description,@NotNull final String projectName) {
        Project project = findOne(id, userId);
        if (project==null)return;
        project.setName(projectName);
        project.setDescription(description);
    }
}
