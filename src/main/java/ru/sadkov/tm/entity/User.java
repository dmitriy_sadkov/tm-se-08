package ru.sadkov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.sadkov.tm.enumeration.Role;
import ru.sadkov.tm.util.RandomUtil;

@Getter
@Setter
public class User extends AbstractEntity {
    private String login;
    private String password;
    private Role role;

    public User(String login, String password, Role role) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.id = RandomUtil.UUID();
    }
/*
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
*/
    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", login='" + login + '\'' +
                ", role=" + role +
                '}';
    }
/*
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }*/
}
