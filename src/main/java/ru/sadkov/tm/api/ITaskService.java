package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Task;
import ru.sadkov.tm.entity.User;

import java.util.List;

public interface ITaskService {
    @NotNull
    ITaskRepository getTaskRepository();

    void setTaskRepository(@NotNull ITaskRepository taskRepository);

    @Nullable
    Task findTaskByName(@Nullable String taskName, @Nullable String userId);

    void saveTask(@Nullable String taskName, @Nullable String projectName, @Nullable User currentUser);

    void removeTask(@Nullable String taskName, @Nullable User currentUser);

    @Nullable
    List<Task> findAll(@Nullable User user);

    void clearTasks(@Nullable User user);

    void removeTaskForProject(@Nullable String projectName, @Nullable User currentUser);

    void update(@Nullable String oldName, @Nullable String newName, @Nullable String userId);

}
