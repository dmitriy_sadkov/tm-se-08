package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;

import java.util.Collection;

public interface IProjectRepository extends IRepository<Project> {
    @NotNull
    Collection<Project> findAll(@NotNull String userId);

    void remove(@NotNull String userId, @NotNull String projectName);

    void removeAll(@NotNull String userId);

    @Nullable
    Project findOne(@NotNull String projectId, @NotNull String userId);

    boolean merge(@NotNull Project project);

    void update(@NotNull String userId, @NotNull String id, @NotNull String description, @NotNull String projectName);

    @Nullable
    String findProjectIdByName(@NotNull String name, @NotNull String userId);

}
