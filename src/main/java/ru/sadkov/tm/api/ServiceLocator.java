package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.sadkov.tm.command.AbstractCommand;

import java.util.List;
import java.util.Map;
import java.util.Scanner;

public interface ServiceLocator {
    @NotNull
    IUserService getUserService();

    void setUserService(IUserService userService);

    @NotNull
    List<AbstractCommand> getCommandList();

    @NotNull
    Scanner getScanner();

    @NotNull
    IProjectService getProjectService();

    void setProjectService(IProjectService projectService);

    @NotNull
    ITaskService getTaskService();

    void setTaskService(ITaskService taskService);
}
