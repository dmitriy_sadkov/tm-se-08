package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.User;


import java.security.NoSuchAlgorithmException;

public interface IUserService {
    @Nullable
    User getCurrentUser();

    void setCurrentUser(@Nullable User currentUser);

    void userRegister(@Nullable User user) throws NoSuchAlgorithmException;

    boolean login(@Nullable String login, @Nullable String password) throws NoSuchAlgorithmException;

    void logout();

    void updatePassword(@Nullable String newPassword) throws NoSuchAlgorithmException;

    @Nullable
    User showUser();

    boolean updateProfile(@Nullable String newUserName);

    void addTestUsers() throws NoSuchAlgorithmException;

    boolean isAuth();
}
