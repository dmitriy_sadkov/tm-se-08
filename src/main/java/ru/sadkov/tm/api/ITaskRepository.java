package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {
    @NotNull
    List<Task> findAll(@NotNull String userId);

    void removeAll(@NotNull String userId);

    @Nullable
    Task findOne(@NotNull String taskId, @NotNull String userId);

    void merge(@NotNull String taskName, @NotNull String projectId, @NotNull String userId);

    void update(@NotNull String taskId, @NotNull String taskName, @NotNull String userId);

    @Nullable
    Task findTaskByName(@NotNull String taskName, @NotNull String userId);

    void removeByName(@NotNull String taskName, @NotNull String userId);
}
