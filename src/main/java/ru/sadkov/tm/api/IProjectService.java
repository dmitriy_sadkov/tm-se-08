package ru.sadkov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.sadkov.tm.entity.Project;
import ru.sadkov.tm.entity.User;
import ru.sadkov.tm.repository.ProjectRepository;

import java.util.List;

public interface IProjectService {
    @Nullable
    String findProjectIdByName(@Nullable String projectName, @Nullable String userId);

    boolean saveProject(@Nullable String projectName, @Nullable String userId);

    void removeByName(@Nullable String projectName, @Nullable String userId);

    @Nullable
    List<Project> showProjectsForUser(@Nullable User user);

    void clearProjects(@Nullable User user);

    @NotNull
    IProjectRepository getProjectRepository();

    void setProjectRepository(@NotNull ProjectRepository projectRepository);

    void update(@Nullable Project project, @Nullable String projectName, @Nullable String description);

    @Nullable
    Project findOneByName(@Nullable String projectName, @Nullable User currentUser);
}
